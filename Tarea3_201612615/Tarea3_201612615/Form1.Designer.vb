﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Colores
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.Amarillo1 = New System.Windows.Forms.RadioButton()
        Me.Azul1 = New System.Windows.Forms.RadioButton()
        Me.Rojo1 = New System.Windows.Forms.RadioButton()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.Amarillo2 = New System.Windows.Forms.RadioButton()
        Me.Rojo2 = New System.Windows.Forms.RadioButton()
        Me.Azul2 = New System.Windows.Forms.RadioButton()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.TextBox1 = New System.Windows.Forms.TextBox()
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.SuspendLayout()
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.Amarillo1)
        Me.GroupBox1.Controls.Add(Me.Azul1)
        Me.GroupBox1.Controls.Add(Me.Rojo1)
        Me.GroupBox1.Location = New System.Drawing.Point(57, 42)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(430, 110)
        Me.GroupBox1.TabIndex = 10
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Colores 1"
        '
        'Amarillo1
        '
        Me.Amarillo1.AutoSize = True
        Me.Amarillo1.ForeColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Amarillo1.Location = New System.Drawing.Point(304, 47)
        Me.Amarillo1.Name = "Amarillo1"
        Me.Amarillo1.Size = New System.Drawing.Size(79, 21)
        Me.Amarillo1.TabIndex = 2
        Me.Amarillo1.TabStop = True
        Me.Amarillo1.Text = "Amarillo"
        Me.Amarillo1.UseVisualStyleBackColor = True
        '
        'Azul1
        '
        Me.Azul1.AutoSize = True
        Me.Azul1.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.Azul1.Location = New System.Drawing.Point(186, 47)
        Me.Azul1.Name = "Azul1"
        Me.Azul1.Size = New System.Drawing.Size(56, 21)
        Me.Azul1.TabIndex = 1
        Me.Azul1.TabStop = True
        Me.Azul1.Text = "Azul"
        Me.Azul1.UseVisualStyleBackColor = True
        '
        'Rojo1
        '
        Me.Rojo1.AutoSize = True
        Me.Rojo1.ForeColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Rojo1.Location = New System.Drawing.Point(53, 47)
        Me.Rojo1.Name = "Rojo1"
        Me.Rojo1.Size = New System.Drawing.Size(58, 21)
        Me.Rojo1.TabIndex = 0
        Me.Rojo1.TabStop = True
        Me.Rojo1.Text = "Rojo"
        Me.Rojo1.UseVisualStyleBackColor = True
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.Amarillo2)
        Me.GroupBox2.Controls.Add(Me.Rojo2)
        Me.GroupBox2.Controls.Add(Me.Azul2)
        Me.GroupBox2.Location = New System.Drawing.Point(57, 183)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(430, 110)
        Me.GroupBox2.TabIndex = 10
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Colores 2"
        '
        'Amarillo2
        '
        Me.Amarillo2.AutoSize = True
        Me.Amarillo2.ForeColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Amarillo2.Location = New System.Drawing.Point(304, 46)
        Me.Amarillo2.Name = "Amarillo2"
        Me.Amarillo2.Size = New System.Drawing.Size(79, 21)
        Me.Amarillo2.TabIndex = 5
        Me.Amarillo2.TabStop = True
        Me.Amarillo2.Text = "Amarillo"
        Me.Amarillo2.UseVisualStyleBackColor = True
        '
        'Rojo2
        '
        Me.Rojo2.AutoSize = True
        Me.Rojo2.ForeColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Rojo2.Location = New System.Drawing.Point(53, 46)
        Me.Rojo2.Name = "Rojo2"
        Me.Rojo2.Size = New System.Drawing.Size(58, 21)
        Me.Rojo2.TabIndex = 3
        Me.Rojo2.TabStop = True
        Me.Rojo2.Text = "Rojo"
        Me.Rojo2.UseVisualStyleBackColor = True
        '
        'Azul2
        '
        Me.Azul2.AutoSize = True
        Me.Azul2.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.Azul2.Location = New System.Drawing.Point(186, 46)
        Me.Azul2.Name = "Azul2"
        Me.Azul2.Size = New System.Drawing.Size(56, 21)
        Me.Azul2.TabIndex = 4
        Me.Azul2.TabStop = True
        Me.Azul2.Text = "Azul"
        Me.Azul2.UseVisualStyleBackColor = True
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(84, 339)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(162, 36)
        Me.Button1.TabIndex = 2
        Me.Button1.Text = "Mezclar"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'TextBox1
        '
        Me.TextBox1.BackColor = System.Drawing.Color.LightGray
        Me.TextBox1.Enabled = False
        Me.TextBox1.Location = New System.Drawing.Point(286, 339)
        Me.TextBox1.Multiline = True
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.Size = New System.Drawing.Size(164, 36)
        Me.TextBox1.TabIndex = 10
        '
        'Colores
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(556, 417)
        Me.Controls.Add(Me.TextBox1)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.GroupBox1)
        Me.Name = "Colores"
        Me.Text = "Colores"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents GroupBox1 As GroupBox
    Friend WithEvents GroupBox2 As GroupBox
    Friend WithEvents Button1 As Button
    Friend WithEvents TextBox1 As TextBox
    Friend WithEvents Amarillo1 As RadioButton
    Friend WithEvents Azul1 As RadioButton
    Friend WithEvents Rojo1 As RadioButton
    Public WithEvents Amarillo2 As RadioButton
    Public WithEvents Rojo2 As RadioButton
    Public WithEvents Azul2 As RadioButton
End Class
