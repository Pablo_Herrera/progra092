﻿Module Module1

    Sub Main()
        Dim N1, N2, R As Double
        Dim Opcion As Char = ""
        While Opcion <> "5"
            Console.Clear()
            Console.WriteLine("Calculadora Basica")
            Console.WriteLine("1. Sumar")
            Console.WriteLine("2. Restar")
            Console.WriteLine("3. Multiplicar")
            Console.WriteLine("4. Dividir")
            Console.WriteLine("5. Salir")
            Try
                Console.Write("Ingrese la operacion que quiere realizar: ")
                Opcion = Console.ReadKey.KeyChar
                Console.WriteLine()
                Select Case Opcion
                    Case "1"
                        Console.WriteLine("Se realizara una suma")
                        Console.Write("Ingresar el numero 1: ")
                        N1 = Console.ReadLine
                        Console.Write("Ingresar el numero 2: ")
                        N2 = Console.ReadLine
                        R = N1 + N2
                        Console.WriteLine("El resultado de la Suma es: " & R)
                        Console.WriteLine("Para continuar, presione una tecla")
                        Console.ReadKey()
                    Case "2"
                        Console.WriteLine("Se realizara una resta")
                        Console.Write("Ingresar el numero 1: ")
                        N1 = Console.ReadLine
                        Console.Write("Ingresar el numero 2: ")
                        N2 = Console.ReadLine
                        R = N1 - N2
                        Console.WriteLine("El resultado de la Resta es: " & R)
                        Console.WriteLine("Para continuar, presione una tecla")
                        Console.ReadKey()
                    Case "3"
                        Console.WriteLine("Se realizara una multiplicacion")
                        Console.Write("Ingresar el numero 1: ")
                        N1 = Console.ReadLine
                        Console.Write("Ingresar el numero 2: ")
                        N2 = Console.ReadLine
                        R = N1 * N2
                        Console.WriteLine("El resultado de la Multiplicacion es: " & R)
                        Console.WriteLine("Para continuar, presione una tecla")
                        Console.ReadKey()
                    Case "4"
                        Console.WriteLine("Se realizara una division")
                        Console.Write("Ingresar el numero 1: ")
                        N1 = Console.ReadLine
                        Console.Write("Ingresar el numero 2: ")
                        N2 = Console.ReadLine
                        If N2 <> 0 Then
                            R = N1 / N2
                            Console.WriteLine("El resultado de la Suma es: " & R)
                        Else
                            Console.WriteLine("El divisor no puede ser 0, ya que es indeterminado")
                        End If
                        Console.WriteLine("Para continuar, presione una tecla")
                        Console.ReadKey()
                    Case "5"
                        Console.WriteLine("Gracias por usar la calculadora")
                        Console.ReadKey()
                    Case Else
                        Console.WriteLine("Opcion no valida, seleccione otra opcion")
                        Console.ReadKey()
                End Select
            Catch ex As Exception
                Console.WriteLine(ex.Message)
                Console.ReadKey()
            End Try
        End While
    End Sub

End Module
