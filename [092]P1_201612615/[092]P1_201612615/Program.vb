Imports System
Imports System.IO

Module Program
    Sub Main()
        Dim encabezado As String = "--- Parcial 1 " & vbLf & "--- Pablo Andr�s Herrera Contreras " & vbLf & "--- 201612615"
        Dim opcion As Char = ""
        Dim objStreamWriter As StreamWriter

        While opcion <> "7"
            Try
                Console.WriteLine(encabezado)
                Console.WriteLine(vbLf & vbLf & "OPCIONES DISPONIBLES:")
                Console.WriteLine("1. Terminacion 0 y 1")
                Console.WriteLine("2. Terminacion 2 y 3")
                Console.WriteLine("3. Terminacion 4 y 5")
                Console.WriteLine("4. Terminacion 6 y 7")
                Console.WriteLine("5. Terminacion 8")
                Console.WriteLine("6. Terminacion 9")
                Console.WriteLine("7. Salir")
                Console.Write(vbLf & "Escribir la opcion segun su terminacion de carnet: ")
                opcion = Console.ReadKey.KeyChar
                Console.Clear()
                Select Case opcion
                    Case "1"
                        Console.WriteLine(encabezado)
                        Console.WriteLine(vbLf & vbLf & "TERMINACION 0 y 1")
                        Dim multiplicador, inicio, fin As Integer
                        Console.Write("Ingrese el numero al que le desea saber la tabla: ")
                        multiplicador = Console.ReadLine
                        Console.Write("Ingrese el numero donde iniciara la tabla: ")
                        inicio = Console.ReadLine
                        Console.Write("Ingrese el numero donde finalizara la tabla: ")
                        fin = Console.ReadLine
                        objStreamWriter = New StreamWriter("..\..\Terminacion0y1.txt")
                        Console.WriteLine()
                        Console.WriteLine(vbLf & vbLf & "TABLA DE MULTIPLICAR DE: " & multiplicador)
                        objStreamWriter.WriteLine("TABLA DE MULTIPLICAR DE: " & multiplicador)
                        Console.WriteLine("Inicia en: " & inicio & " Y termina en: " & fin)
                        objStreamWriter.WriteLine("Inicia en: " & inicio & " Y termina en: " & fin)
                        For i As Integer = inicio To fin Step 1
                            Console.WriteLine(multiplicador & " x " & i & " = " & (multiplicador * i))
                            objStreamWriter.WriteLine(multiplicador & " x " & i & " = " & (multiplicador * i))
                        Next
                        objStreamWriter.Close()
                    Case "2"
                        Console.WriteLine(encabezado)
                        Console.WriteLine(vbLf & vbLf & "TERMINACION 2 y 3")
                        Dim palabra As String
                        Dim ca, ce, ci, co, cu As Integer
                        ca = ce = ci = co = cu = 0
                        Console.Write("Ingrese la palabra al que le desea encontrar las vocales: ")
                        palabra = Console.ReadLine
                        For i As Integer = 0 To (palabra.Length - 1) Step 1
                            If palabra.Substring(i, 1).ToLower = "a" Then
                                ca = ca + 1
                            ElseIf palabra.Substring(i, 1).ToLower = "e" Then
                                ce = ce + 1
                            ElseIf palabra.Substring(i, 1).ToLower = "i" Then
                                ci = ci + 1
                            ElseIf palabra.Substring(i, 1).ToLower = "o" Then
                                co = co + 1
                            ElseIf palabra.Substring(i, 1).ToLower = "u" Then
                                cu = cu + 1
                            End If
                        Next
                        objStreamWriter = New StreamWriter("..\..\Terminacion2y3.txt")
                        Console.WriteLine(vbLf & vbLf & "Palabra ingresada: " & palabra)
                        objStreamWriter.WriteLine("Palabra ingresada: " & palabra)
                        Console.WriteLine("Cantidad de a: " & ca)
                        objStreamWriter.WriteLine("Cantidad de a: " & ca)
                        Console.WriteLine("Cantidad de e: " & ce)
                        objStreamWriter.WriteLine("Cantidad de e: " & ce)
                        Console.WriteLine("Cantidad de i: " & ci)
                        objStreamWriter.WriteLine("Cantidad de i: " & ci)
                        Console.WriteLine("Cantidad de o: " & co)
                        objStreamWriter.WriteLine("Cantidad de o: " & co)
                        Console.WriteLine("Cantidad de u: " & cu)
                        objStreamWriter.WriteLine("Cantidad de u: " & cu)
                        objStreamWriter.Close()
                    Case "3"
                        Console.WriteLine(encabezado)
                        Console.WriteLine(vbLf & vbLf & "TERMINACION 4 y 5")
                        Dim palabra1, palabra2, palabra3, palabramayor As String
                        Dim i As Integer
                        palabramayor = ""
                        objStreamWriter = New StreamWriter("..\..\Terminacion4y5.txt")
                        Console.Write("Escriba la primera palabra: ")
                        palabra1 = Console.ReadLine
                        Console.Write("Escriba la segunda palabra: ")
                        palabra2 = Console.ReadLine
                        Console.Write("Escriba la tercera palabra: ")
                        palabra3 = Console.ReadLine

                        Dim palabras() As String = {palabra1, palabra2, palabra3}
                        For i = 0 To (palabras.Count - 1) Step 1
                            objStreamWriter.WriteLine("Palabra ingresada: " & palabras(i))
                            If palabras(i).Length > palabramayor.Length Then
                                palabramayor = palabras(i)
                            End If
                        Next

                        Console.WriteLine(vbLf + "La palabra mas grande es: " & palabramayor)
                        objStreamWriter.WriteLine(vbLf + "La palabra mas grande es: " & palabramayor)
                        objStreamWriter.Close()

                    Case "4"
                        Console.WriteLine(encabezado)
                        Console.WriteLine(vbLf & vbLf & "TERMINACION 6 y 7")
                        Dim inicio, fin As Integer
                        inicio = fin = 0
                        While fin - inicio <> 10
                            Console.Write("Ingrese el inicio del rango de numeros a analizar: ")
                            inicio = Console.ReadLine
                            Console.Write("Ingrese el fin del rango de numeros a analizar: ")
                            fin = Console.ReadLine
                            If (fin - inicio <> 10) Then
                                Console.WriteLine("El rango de numeros debe de ser de 10 numeros")
                            End If
                        End While
                        objStreamWriter = New StreamWriter("..\..\Terminacion6y7.txt")
                        Console.WriteLine()
                        For i As Integer = inicio To fin Step 1
                            Dim contador As Integer = 0
                            Dim j As Integer
                            For j = 1 To i Step 1
                                If i Mod j = 0 Then
                                    contador = contador + 1
                                End If
                            Next
                            If contador = 2 Then
                                Console.WriteLine("Numero: " & i & " Es primo")
                                objStreamWriter.WriteLine("Numero: " & i & " Es primo")
                            Else
                                Console.WriteLine("Numero: " & i & " No es primo")
                                objStreamWriter.WriteLine("Numero: " & i & " No es Primo")
                            End If

                        Next
                        objStreamWriter.Close()
                    Case "5"
                        Console.WriteLine(encabezado)
                        Console.WriteLine(vbLf & vbLf & "TERMINACION 8")
                        Dim mensaje As String = ""
                        Dim i As Integer = 0
                        While mensaje = ""
                            Console.Write("Ingrese la palabra que desea escribir al reves: ")
                            mensaje = Console.ReadLine
                            If mensaje = "" Then
                                Console.WriteLine("La palabra no puede irse en blanco.")
                            End If
                        End While
                        objStreamWriter = New StreamWriter("..\..\Terminacion8.txt")
                        objStreamWriter.WriteLine("Palabra para escribir al reves: " & mensaje)
                        Console.Write("Palabra escrita al reves: ")
                        objStreamWriter.Write("Palabra escrita al reves: ")
                        For i = mensaje.Length - 1 To 0 Step -1
                            Console.Write(mensaje.Substring(i, 1))
                            objStreamWriter.Write(mensaje.Substring(i, 1))
                        Next
                        objStreamWriter.Close()
                    Case "6"
                        Console.WriteLine(encabezado)
                        Console.WriteLine(vbLf & vbLf & "TERMINACION 9")
                        Dim cantidad As Integer = 0
                        Dim contador As Integer = 0
                        Dim numero As Integer = 1
                        Dim contador2 As Integer = 0
                        Dim i As Integer
                        Console.Write("Ingrese la cantidad de numeros primos que quiere saber: ")
                        cantidad = Console.ReadLine

                        objStreamWriter = New StreamWriter("..\..\Terminacion9.txt")
                        Console.WriteLine("Listado de numeros primos")
                        objStreamWriter.WriteLine("Listado de numeros primos")
                        While contador <> cantidad
                            contador2 = 0
                            For i = 1 To numero Step 1
                                If numero Mod i = 0 Then
                                    contador2 = contador2 + 1
                                End If
                            Next
                            If contador2 = 2 Then
                                Console.WriteLine((contador + 1) & ". " & numero)
                                objStreamWriter.WriteLine((contador + 1) & ". " & numero)
                                contador = contador + 1
                            End If
                            numero = numero + 1
                        End While
                        objStreamWriter.Close()
                            Case "7"
                        Console.WriteLine(encabezado)
                        Console.WriteLine(vbLf & vbLf & "El programa se ha finalizado exitosamente, presione cualquier tecla para salir.")
                        Console.ReadKey()
                        Console.Clear()
                    Case Else
                        Console.WriteLine(encabezado)
                        Console.WriteLine(vbLf & vbLf & "--- ERROR: La opcion escogida no es valida, para regresar presione cualquier tecla.")
                        Console.ReadKey()
                        Console.Clear()
                End Select
                If opcion <> "7" Then
                    Console.WriteLine(vbLf & vbLf & "Se guardo el archivo en la carpeta del programa.")
                    Console.WriteLine("Para regresar al menu principal, presine cualquier tecla.")
                    Console.ReadKey()
                    Console.Clear()
                End If

            Catch ex As Exception
                Console.WriteLine("--- ERROR: El error fue debido a: " & ex.Message)
                Console.WriteLine("Para regresar presione cualquier tecla:")
                Console.ReadKey()
                Console.Clear()
            End Try
        End While
    End Sub
End Module