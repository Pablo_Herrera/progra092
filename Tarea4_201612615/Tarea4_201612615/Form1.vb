﻿Imports System.IO

Public Class Form1
    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Dim n1, n2 As Double
        Try
            Dim suma, resta, multi, divi As Double
            Dim ts, tr, tm, td As String
            n1 = Val(TextBox1.Text)
            n2 = Val(TextBox2.Text)
            suma = n1 + n2
            resta = n1 - n2
            multi = n1 * n2
            If (n2 < 0) Then
                td = "El resultado de la division es: Error"
            Else
                divi = n1 / n2
                td = "El resultado de la suma es: " & divi
            End If
            ts = "El resultado de la suma es: " & suma
            tr = "El resultado de la resta es: " & resta
            tm = "El resultado de la multiplicacion es: " & multi
            MsgBox("Resultados" & vbLf & ts & vbLf & tr & vbLf & tm & vbLf & td)
        Catch ex As Exception
            MsgBox("No se puede dividir dentro de 0")
        End Try
    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        TextBox1.Text = ""
        TextBox2.Text = ""
        MsgBox("Se limpio correctamente los campos")
    End Sub

    Private Sub Button3_Click(sender As Object, e As EventArgs) Handles Button3.Click
        Dim n1, n2 As Double
        Try
            Dim suma, resta, multi, divi As Double
            Dim ts, tr, tm, td As String
            n1 = Val(TextBox1.Text)
            n2 = Val(TextBox2.Text)
            suma = n1 + n2
            resta = n1 - n2
            multi = n1 * n2
            If (n2 < 0) Then
                td = "El resultado de la division es: Error"
            Else
                divi = n1 / n2
                td = "El resultado de la suma es: " & divi
            End If
            ts = "El resultado de la suma es: " & suma
            tr = "El resultado de la resta es: " & resta
            tm = "El resultado de la multiplicacion es: " & multi
            Dim objStreamWriter As StreamWriter
            objStreamWriter = New StreamWriter("..\..\Resultados.txt")
            objStreamWriter.WriteLine("Resultados" & vbLf & ts & vbLf & tr & vbLf & tm & vbLf & td)
            MsgBox("Se guardo el archivo correctamente")
            objStreamWriter.Close()

        Catch ex As Exception
            MsgBox("No se puede dividir dentro de 0")
        End Try
    End Sub

    Private Sub Button4_Click(sender As Object, e As EventArgs) Handles Button4.Click
        MsgBox("Cerrando el programa...")
        Me.Close()

    End Sub
End Class


