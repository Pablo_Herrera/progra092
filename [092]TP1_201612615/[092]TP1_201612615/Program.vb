Imports System
Imports System.IO

Module Program
    Sub Main()
        Dim encabezado As String = "--- Tarea Preparatoria / Parcial 1 " & vbLf & "--- Pablo Andr�s Herrera Contreras " & vbLf & "--- 201612615"
        Dim opcion As Char = ""
        While opcion <> "6"
            Try
                Console.WriteLine(encabezado)
                Console.WriteLine(vbLf & vbLf & "OPCIONES DISPONIBLES:")
                Console.WriteLine("1. Tabla de Multiplicar")
                Console.WriteLine("2. Divisores")
                Console.WriteLine("3. Vocales")
                Console.WriteLine("4. Letra elegida")
                Console.WriteLine("5. Grabar en archivo de texto")
                Console.WriteLine("6. Salir")
                Console.Write(vbLf & "Escribir el numero de la opcion que desea utilizar: ")
                opcion = Console.ReadKey.KeyChar
                Console.Clear()
                Select Case opcion
                    Case "1"
                        Console.WriteLine(encabezado)
                        Console.WriteLine(vbLf & vbLf & "TABLAS DE MULTIPLICAR")
                        Dim multiplicador, limite As Integer
                        Console.Write("Ingrese el numero al que le desea saber la tabla: ")
                        multiplicador = Console.ReadLine
                        Console.Write("Ingrese el numero hasta que desea saber la tabla: ")
                        limite = Console.ReadLine
                        Console.Clear()
                        Console.WriteLine(encabezado)
                        Console.WriteLine(vbLf & vbLf & "TABLA DE MULTIPLICAR DE: " & multiplicador)
                        For i As Integer = 1 To limite Step 1
                            Console.WriteLine(multiplicador & " x " & i & " = " & (multiplicador * i))
                        Next
                        Console.WriteLine(vbLf & vbLf & "Para regresar al menu principal, presine cualquier tecla.")
                        Console.ReadKey()
                        Console.Clear()
                    Case "2"
                        Console.WriteLine(encabezado)
                        Console.WriteLine(vbLf & vbLf & "DIVISORES")
                        Dim divisor As Integer
                        Console.Write("Ingrese el numero al que le desea saber los divisores: ")
                        divisor = Console.ReadLine
                        Console.Write("La lista de divisores es: ")
                        For i As Integer = 1 To divisor Step 1
                            If divisor Mod i = 0 Then
                                Console.Write(i & " ")
                            End If
                        Next
                        Console.WriteLine(vbLf & vbLf & "Para regresar al menu principal, presine cualquier tecla.")
                        Console.ReadKey()
                        Console.Clear()
                    Case "3"
                        Console.WriteLine(encabezado)
                        Console.WriteLine(vbLf & vbLf & "VOCALES")
                        Dim mensaje As String
                        Console.Write("Ingrese el mensaje al que le desea encontrar las vocales: ")
                        mensaje = Console.ReadLine
                        Dim contador As Integer = 0
                        For i As Integer = 0 To (mensaje.Length - 1) Step 1
                            If mensaje.Substring(i, 1).ToLower = "a" Or
                               mensaje.Substring(i, 1).ToLower = "e" Or
                               mensaje.Substring(i, 1).ToLower = "i" Or
                               mensaje.Substring(i, 1).ToLower = "o" Or
                               mensaje.Substring(i, 1).ToLower = "u" Then
                                contador = contador + 1
                            End If
                        Next
                        Console.WriteLine("La cantidad de vocales en la frase es de: " & contador)
                        Console.WriteLine(vbLf & vbLf & "Para regresar al menu principal, presine cualquier tecla.")
                        Console.ReadKey()
                        Console.Clear()
                    Case "4"
                        Console.WriteLine(encabezado)
                        Console.WriteLine(vbLf & vbLf & "LETRA ELEGIDA")
                        Dim mensaje As String
                        Dim letra As Char
                        Console.Write("Ingrese el mensaje que se va a analizar: ")
                        mensaje = Console.ReadLine
                        Console.Write("Ingrese la letra que quiere encontrar en la frase: ")
                        letra = Console.ReadKey.KeyChar
                        Dim contador As Integer = 0
                        For i As Integer = 0 To (mensaje.Length - 1) Step 1
                            If mensaje.Substring(i, 1).ToLower = letra.ToString.ToLower Then
                                contador = contador + 1
                            End If
                        Next
                        Console.WriteLine(vbLf & vbLf & "La cantidad de la letra " & letra.ToString & " en la frase es de: " & contador)
                        Console.WriteLine(vbLf & vbLf & "Para regresar al menu principal, presine cualquier tecla.")
                        Console.ReadKey()
                        Console.Clear()
                    Case "5"
                        Console.WriteLine(encabezado)
                        Console.WriteLine(vbLf & vbLf & "GRABAR ARCHIVO DE TEXTO")
                        Dim mensaje As String
                        Dim nombre As String = ""
                        Console.Write("Ingrese el texto que quiere almacenar en un archivo texto: ")
                        mensaje = Console.ReadLine
                        Dim objStreamWriter As StreamWriter
                        While nombre = ""
                            Console.Write("Ingrese el nombre del archivo txt a guardar: ")
                            nombre = Console.ReadLine
                        End While
                        objStreamWriter = New StreamWriter("..\..\..\" & nombre & ".txt")
                        objStreamWriter.WriteLine(mensaje)
                        objStreamWriter.Close()
                        Console.WriteLine("El archivo fue almacenado exitosamente, revisar en la carpeta del archivo fuente.")
                        Console.WriteLine(vbLf & vbLf & "Para regresar al menu principal, presine cualquier tecla.")
                        Console.ReadKey()
                        Console.Clear()
                    Case "6"
                        Console.WriteLine(encabezado)
                        Console.WriteLine(vbLf & vbLf & "El programa se ha finalizado exitosamente, presione cualquier tecla para salir.")
                        Console.ReadKey()
                        Console.Clear()
                    Case Else
                        Console.WriteLine(encabezado)
                        Console.WriteLine(vbLf & vbLf & "--- ERROR: La opcion escogida no es valida, para regresar presione cualquier tecla.")
                        Console.ReadKey()
                        Console.Clear()
                End Select
            Catch ex As Exception
                Console.WriteLine("--- ERROR: El error fue debido a: " & ex.Message)
                Console.WriteLine("Para regresar presione cualquier tecla:")
                Console.ReadKey()
                Console.Clear()
            End Try
        End While
    End Sub
End Module
